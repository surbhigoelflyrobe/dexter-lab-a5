import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '../../../../node_modules/@angular/material';
import { CreateCampaignModalComponent } from '../../modals/create-campaign-modal/create-campaign-modal.component';
import { Router } from '../../../../node_modules/@angular/router';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-campaigns',
  templateUrl: './campaigns.component.html',
  styleUrls: ['./campaigns.component.css']
})
export class CampaignsComponent implements AfterViewInit {

  event_data = [];
  event_data_count: any;
  dataSource: any;
  spinner = true;
  paginationPageSize = 10;
  page_size = 10;
  tableData = [];

  columnDefs = [
    {headerName: 'Campaign Name', field: 'campaign_name'},
    {headerName: 'Campaign Type', field: 'campaign_type'},
    {headerName: 'Status', field: 'is_active', width : 80},
    {headerName: 'PN Delivered', field: 'pn_delivered', width : 120},
    {headerName: 'PN Clicked', field: 'pn_clicked', width : 100},
    {headerName: 'Start Date', field: 'start_date_time', width : 130},
    {headerName: 'End Date', field: 'end_date_time', width : 140}
    ];


  constructor(
    private api: ApiService,
    private dialog: MatDialog,
    private router: Router,
    private $root: SharedService
  ) {
    this.getCampaigns(this.paginationPageSize);
  }

  fireCampaignCreation() {
    const modalRef = this.dialog.open(CreateCampaignModalComponent, {
      width: '650px',
      data: {}
    });

    modalRef.afterClosed().subscribe(data => {
      if (data.data) {
        this.$root.create_campaign_type = data.data;
        this.router.navigate(['home/create-campaign/' + data.data]);
      }
    });
  }

  fireEditAction(data) {
    this.$root.edit_data = data;
    this.router.navigate(['home/edit-campaign']);
  }

  ngAfterViewInit() {
  }

  getCampaigns(size = 20){
    this.api.getCampaigns(size).subscribe(res => {

      this.event_data = [];
      this.tableData = [];

      res['data'].forEach(element => {
        element.start_date_time = element.start_date.date + " " + element.start_date.time + " " + element.start_date.meridiem;
        
        if(element.end_date.date == undefined){
          element.end_date_time = "";
        }
        else{
          element.end_date_time = element.end_date.date + " " + element.end_date.time + " " + element.end_date.meridiem;
        }
        
        this.event_data.push(element);
        this.tableData.push(element);
      });

      this.event_data_count = res['data'].length;
      this.spinner = false;

      // if(res['next_url']){
      //   this.getNextCampaignList(res['next_url']);
      // }
      
    });
  }
  
  getNextCampaignList(url){

    this.api.getNextCampaignList(url).subscribe(res => {
      res['data'].forEach(element => {
        element.start_date_time = element.start_date.date + " " + element.start_date.time + " " + element.start_date.meridiem;
      element.end_date_time = element.end_date.date + " " + element.end_date.time + " " + element.end_date.meridiem;
        this.tableData.push(element);
      });

      if(res['next_url']){
        this.getNextCampaignList(res['next_url']);
      }
      else{
        // this.dataSource = new MatTableDataSource(this.tableData);
        // this.dataSource.paginator = this.paginator;
        // this.dataSource.sort = this.sort;
      }

    });

  }

  onPageSizeChanged(){
    this.paginationPageSize = this.page_size;
    this.getCampaigns(this.paginationPageSize);
  }
}
