import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';
import { Router } from '../../../../node_modules/@angular/router';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-segments',
  templateUrl: './segments.component.html',
  styleUrls: ['./segments.component.css']
})
export class SegmentsComponent implements OnInit {

  dataSource: any;
  // displayedColumns = ['name', 'users', 'email', 'sms', 'push', 'created', 'action'];
  displayedColumns = ['name', 'email', 'sms', 'push', 'created'];
  tableData = [];
  spinner = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private api: ApiService,
    private router: Router,
    private $root: SharedService
  ) { }

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  fireSegmentCreation() {
    this.router.navigate(['home/create-segment']);
  }

  fireEditAction(data){
    this.router.navigate(['home/edit-segment/' + data.segment_name]);
  }

  fireCloneAction(data) {
    this.router.navigate(['home/create-segment/' + data.segment_name]);
  }

  ngOnInit() {
    this.api.getSegments().subscribe(res => {
      this.dataSource = new MatTableDataSource(res['data']);
      this.tableData = res['data'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinner = false;

      if(res['next_url']){
        this.getNextSegmentList(res['next_url']);
      }

    });
  }

  getNextSegmentList(url){

    this.api.getNextSegmentList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.tableData.push(element);
      });

      if(res['next_url']){
        this.getNextSegmentList(res['next_url']);
      }
      else{
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    });

  }

}
