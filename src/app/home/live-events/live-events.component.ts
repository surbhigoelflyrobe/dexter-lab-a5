import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../api.service';
import { MatTableDataSource, MatPaginator, MatSort } from '../../../../node_modules/@angular/material';
import {animate, state, style, transition, trigger} from '@angular/animations';


@Component({
  selector: 'app-live-events',
  templateUrl: './live-events.component.html',
  styleUrls: ['./live-events.component.css'],
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({height: '0px', minHeight: '0', display: 'none'})),
      state('expanded', style({height: '*'})),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ]
})
export class LiveEventsComponent implements OnInit {
  event_data: any;
  event_data_count: any;
  dataSource: any;
  spinner = true;
  displayedColumns = ['id', 'name', 'progress', 'color'];
  expandedElement: PeriodicElement;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private api: ApiService
  ) { }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  ngOnInit() {
    this.api.getLiveEvents().subscribe(res => {
      this.event_data = res['data'];
      console.log(this.event_data);
      this.event_data_count = res['data'].length;

      this.event_data.forEach(col => {

        col.new_properties = [
          {property_name : 'User ID',value : col.user_properties.Social_id},
          {property_name : 'Name',value : col.user_properties.Name},
          {property_name : 'Gender',value : col.user_properties.Gender},
          {property_name : 'First Name',value : col.user_properties.First_name},
          {property_name : 'Last Name',value : col.user_properties.Last_name},
          {property_name : 'City',value : col.user_properties.City},
          {property_name : 'Email',value : col.user_properties.Email},
          {property_name : 'Mobile',value : col.user_properties.Mobile},
          {property_name : 'Channel',value : col.user_properties.Channel},
          {property_name : 'Flyrobe Analytic version',value : col.platform_properties._flyrobe_analytics_version},
          {property_name : 'Android OS name',value : col.platform_properties._android_os_name},
          {property_name : 'Android OS version',value : col.platform_properties._android_os_version},
          {property_name : 'Android Manufacturer',value : col.platform_properties._android_manufacturer},
          {property_name : 'Android Brand',value : col.platform_properties._android_brand},
          {property_name : 'Android Model',value : col.platform_properties._android_model},
          {property_name : 'Network Carrier',value : col.platform_properties._network_carrier},
          {property_name : 'Network Type',value : col.platform_properties._network_type},
          {property_name : 'Style Type',value : col.event_properties.Style_Type},
          {property_name : 'Dress Type',value : col.event_properties.Dress_Type},
          {property_name : 'Card Name',value : col.event_properties.Card_Name},
          {property_name : 'SD value',value : col.event_properties.SD_value},
          {property_name : 'Rental Period',value : col.event_properties.Rental_Period},
          {property_name : 'Delivery Date',value : col.event_properties.Delivery_Date},
          {property_name : 'Product IDs',value : col.event_properties.Product_IDs},
          {property_name : 'Category',value : col.event_properties.Category},
          {property_name : 'Price',value : col.event_properties.Price},
          {property_name : 'Brand',value : col.event_properties.Brand},
          {property_name : 'Size',value : col.event_properties.Size},
          {property_name : 'Description',value : col.event_properties.Description},
          {property_name : 'Product ID',value : col.event_properties.Product_ID},
          {property_name : 'Pick Up Date',value : col.event_properties.Pick_Up_Date},
          {property_name : 'Filter Detail',value : col.event_properties.Filter_Detail},
          {property_name : 'Rental Value',value : col.event_properties.Rental_Value},
          {property_name : 'Rental_Value',value : col.event_properties.Rental_Value},
          {property_name : 'Security Deposit',value : col.event_properties.Security_Deposit}
    ];
        
      });


      this.dataSource = new MatTableDataSource(this.event_data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinner = false;
    });
  }

}

export interface PeriodicElement {
 
}
