import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';
import { Router } from '../../../../node_modules/@angular/router';
import { SendSmsModalComponent } from '../../modals/send-sms-modal/send-sms-modal.component';
import { SharedService } from '../../shared.service';
import { SmsPreviewComponent } from '../../shared/sms-preview/sms-preview.component';
import { SmsPreviewModalComponent } from '../../modals/sms-preview-modal/sms-preview-modal.component';

@Component({
  selector: 'app-sms',
  templateUrl: './sms.component.html',
  styleUrls: ['./sms.component.css']
})
export class SmsComponent implements OnInit {

  dataSource: any;
  displayedColumns = ['smsName', 'smsContent', 'smsCreated', 'smsAction'];
  tableData = [];
  spinner = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private api: ApiService,
    private router: Router,
    private dialog: MatDialog,
    private $root: SharedService
  ) { }

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  fireSmsCreation() {
    this.router.navigate(['home/create-sms']);
  }

  sendSmsAction(row) {
    const dialogRef = this.dialog.open(SendSmsModalComponent, {
      height: '300px',
      width: '600px',
      data: {
        content: row.template_content,
        name: row.template_name
      }
    });
  }

  firePreviewAction(data) {
    const dialogRef = this.dialog.open(SmsPreviewModalComponent, {
      height: 'auto',
      width: '260px',
      data: data.template_content
    });
  }

  fireCloneAction(data) {
    // this.$root.clone_data = data;
    this.router.navigate(['home/create-sms/' + data.template_name]);
  }

  fireEditAction(data){
    // this.$root.edit_data = data;
    this.router.navigate(['home/edit-sms/' + data.template_name]);
  }

  ngOnInit() {
    this.api.getSmsData().subscribe(res => {
      this.dataSource = new MatTableDataSource(res['data']);
      this.tableData = res['data'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinner = false;

      if(res['next_url']){
        this.getSMSNextList(res['next_url']);
      }

    });
  }

  getSMSNextList(url){

    this.api.getNextSmsList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.tableData.push(element);
      });

      if(res['next_url']){
        this.getSMSNextList(res['next_url']);
      }
      else{
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    });

  }

}
