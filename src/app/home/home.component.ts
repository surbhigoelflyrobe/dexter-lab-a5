import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { SharedService } from '../shared.service';
import { Router } from '../../../node_modules/@angular/router';
import { SwitchModalComponent } from '../modals/switch-modal/switch-modal.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  events: string[] = [];
  opened = true;
  username: any;
  current_route: string;
  mode : number;


  constructor(
    private $root: SharedService,
    private router: Router,
    private dialog: MatDialog
  ) { }

  logoutUser() {
    window.localStorage.removeItem('pnd_token');
    window.localStorage.removeItem('user_data');
    window.localStorage.removeItem('permissions');
    window.localStorage.removeItem('mode');
    this.router.navigate(['/']);
  }

  openSwitchModal(){

    const dialogRef = this.dialog.open(SwitchModalComponent, {
      height: 'auto',
      width: '600px',
      data: {}
    });

  }


  ngOnInit() {
    this.username = this.$root.user_data.first_name + ' ' + this.$root.user_data.last_name ;
    this.current_route = this.router.url;
    // console.log(this.current_route);
    this.router.events.subscribe(events => {
      this.current_route = this.router.url;
    });

    this.mode = parseInt(window.localStorage.getItem('mode'));

  }

}
