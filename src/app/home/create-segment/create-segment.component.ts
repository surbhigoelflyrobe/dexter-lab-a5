import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../api.service';
import { DatePipe } from '../../../../node_modules/@angular/common';
import { MatSnackBar } from '../../../../node_modules/@angular/material';
import { Router , ActivatedRoute } from '../../../../node_modules/@angular/router';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-create-segment',
  templateUrl: './create-segment.component.html',
  styleUrls: ['./create-segment.component.css']
})

// methods
/*
-> saveSegment //to save particular segmentation locally
*/

export class CreateSegmentComponent implements OnInit {
  constructor(
    private api: ApiService,
    private datePipe: DatePipe,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private $root: SharedService
  ) {
    if(this.route.params['value']['name']){
      this.getParticularSegmentData(this.route.params['value']['name']);
  }

  }

  event_list: any;
  city_list: any;
  active_tab = 'recent';
  recent_segment = false;
  lifetime_segment = false;
  demographic_segment = false;
  progress_bar = false;
  edit_mode = false;
  selected_recent_event: any;

  min_date = new Date();
  segment_user_info: any = {};
  save_payload: any = {};
  send_data: any = {};

  creation_payload: any = {
    segment_name: '',
    recent_activity : {
      event_name: '',
      date_range: new Array(2)
    },
    lifetime_activity : {
      field_name: '',
      condition_name: '',
      value: ''
    },
    demographic_activity : {
      gender: '',
      location_name: ''
    }
  };

  /*validateCreationForm() {
    
    let error = false;
    if (
      !this.lifetime_segment &&
      !this.demographic_segment &&
      !this.recent_segment
    ) {
      error = true;
      return error;
    }

    if (this.creation_payload.segment_name === '') {
      error = true;
      return error;
    }

    if (this.recent_segment) {
      if (
        this.creation_payload.recent_activity.date_range[0] === undefined ||
        this.creation_payload.recent_activity.date_range[1] === undefined ||
        this.creation_payload.recent_activity.event_name === ''
      ) {
        error = true;
        return error;
      }
    }

    if (this.lifetime_segment) {
      if (
        this.creation_payload.lifetime_activity.field_name === '' ||
        this.creation_payload.lifetime_activity.condition_name === '' ||
        this.creation_payload.lifetime_activity.value === ''
      ) {
        error = true;
        return error;
      }
    }

    if (this.demographic_segment) {
      if (
        this.creation_payload.demographic_activity.gender === '' &&
        this.creation_payload.demographic_activity.location_name === ''
      ) {
        error = true;
        return error;
      }
    }

    return error;
  }*/

  // START => get information about particular segment
  getParticularSegmentData(template_name){

    this.api.getParticularSegmentData(template_name).subscribe(res => {

      if(this.router.url.indexOf('/home/edit-segment') >= 0){
        this.creation_payload.segment_name = res['data'][0].template_name;
        this.edit_mode = true;
      }
      else{
        this.creation_payload.segment_name = "Clone_of_" + res['data'][0].template_name;
      }


    });


  }
  // END => get information about particular segment

  createSegment() {

    if(!this.creation_payload.segment_name){
      this.snackbar.open('Please enter segment name', 'close', {
        duration: 2000
      });
      return false;
    }

    if(this.save_payload.recent_activity || this.save_payload.lifetime_activity || this.save_payload.demographic_activity){
        this.send_data = {
          segment_name : this.creation_payload.segment_name
        };
      
        if(this.save_payload.recent_activity){
          this.send_data.recent_activity = this.save_payload.recent_activity;
        }

        if(this.save_payload.lifetime_activity){
          this.send_data.lifetime_activity = [this.save_payload.lifetime_activity];
        }

        if(this.save_payload.demographic_activity){

          this.send_data.demographic_activity = {};

          if(this.save_payload.demographic_activity.gender){
            this.send_data.demographic_activity.gender = this.save_payload.demographic_activity.gender;
          }

          if(this.save_payload.demographic_activity.location_name){
            this.send_data.demographic_activity.location_name = this.save_payload.demographic_activity.location_name;
          }

        }

        this.api.createSegment(this.send_data).subscribe(res => {
              if (!res['is_error']) {
                this.snackbar.open('Segment Created Succesfully', 'close', {
                  duration: 2000
                });
                this.router.navigate(['home/segments']);
              } else {
                this.snackbar.open(res['message'], 'close', {
                  duration: 2000
                });
              }
            });

    }
    else{
      this.snackbar.open('Please select atleast one activity.', 'close', {
        duration: 2000
      });
    }

   
  }


  saveSegment(type){

    this.send_data = {};

    if (type === 'recent') {

      if((this.creation_payload.recent_activity.event_name == "" || this.creation_payload.recent_activity.event_name == null || this.creation_payload.recent_activity.event_name === undefined) ||
      (this.creation_payload.recent_activity.date_range[0] == "" || this.creation_payload.recent_activity.date_range[0] == null || this.creation_payload.recent_activity.date_range[0] === undefined) ||
      (this.creation_payload.recent_activity.date_range[1] == "" || this.creation_payload.recent_activity.date_range[1] == null || this.creation_payload.recent_activity.date_range[1] === undefined)){
        this.snackbar.open('Please select all required fields', 'close', {
          duration: 2000
        });
      }
      else{

        this.creation_payload.recent_activity.date_range[0] = this.datePipe.transform(this.creation_payload.recent_activity.date_range[0], 'yyyy-MM-dd');
        this.creation_payload.recent_activity.date_range[1] = this.datePipe.transform(this.creation_payload.recent_activity.date_range[1], 'yyyy-MM-dd');

        // let today_date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

        if(this.creation_payload.recent_activity.date_range[0] === null){
          this.snackbar.open('Please select valid Start Date', 'close', {
            duration: 2000
          });
          return false;
        }

        // if(this.creation_payload.recent_activity.date_range[0] === null || this.creation_payload.recent_activity.date_range[0] < today_date){
        //   this.snackbar.open('Please select valid Start Date', 'close', {
        //     duration: 2000
        //   });
        //   return false;
        // }

        if(this.creation_payload.recent_activity.date_range[1] === null){
          this.snackbar.open('Please select valid End Date', 'close', {
            duration: 2000
          });
          return false;
        }

        // if(this.creation_payload.recent_activity.date_range[1] === null || this.creation_payload.recent_activity.date_range[1] < today_date){
        //   this.snackbar.open('Please select valid End Date', 'close', {
        //     duration: 2000
        //   });
        //   return false;
        // }

        if(this.creation_payload.recent_activity.date_range[0] > this.creation_payload.recent_activity.date_range[1]){
          this.snackbar.open('Please select valid Start and End Date', 'close', {
            duration: 2000
          });
          return false;
        }

        this.save_payload.recent_activity = {
          event_name : this.creation_payload.recent_activity.event_name,
          date_range : [this.creation_payload.recent_activity.date_range[0],this.creation_payload.recent_activity.date_range[1]]
        };

        this.active_tab = 'lifetime';
        this.send_data.recent_activity = this.save_payload.recent_activity;

      }

    }

    else if (type === 'lifetime') {

      if((this.creation_payload.lifetime_activity.field_name == "" || this.creation_payload.lifetime_activity.field_name == null || this.creation_payload.lifetime_activity.field_name === undefined) ||
      (this.creation_payload.lifetime_activity.condition_name == "" || this.creation_payload.lifetime_activity.condition_name == null || this.creation_payload.lifetime_activity.condition_name === undefined) ||
      (this.creation_payload.lifetime_activity.value == "" || this.creation_payload.lifetime_activity.value == null || this.creation_payload.lifetime_activity.value === undefined)){
        this.snackbar.open('Please select all required fields', 'close', {
          duration: 2000
        });
      }
      else{
        this.save_payload.lifetime_activity = {
          field_name : this.creation_payload.lifetime_activity.field_name,
          condition_name : this.creation_payload.lifetime_activity.condition_name,
          value : parseInt(this.creation_payload.lifetime_activity.value)
        };

        this.active_tab = 'demographic';
        this.send_data.lifetime_activity = [this.save_payload.lifetime_activity];

      }

    }

    else if (type === 'demographic') {

      if(this.creation_payload.demographic_activity.gender || this.creation_payload.demographic_activity.location_name){
        
        this.save_payload.demographic_activity = {
          gender : this.creation_payload.demographic_activity.gender,
          location_name : this.creation_payload.demographic_activity.location_name
        };
        this.send_data.demographic_activity = this.save_payload.demographic_activity;

        this.send_data.demographic_activity.location_name = [this.save_payload.demographic_activity.location_name];

      }
      else{
        this.snackbar.open('Please select all required fields', 'close', {
          duration: 2000
        });
      }

    }

    this.progress_bar = true;

    this.api.getSegmentData(this.send_data).subscribe(res => {
      this.progress_bar = false;
      if (!res['is_error']) {
        this.segment_user_info = res['data'];
        this.segment_user_info.segment_email = this.segment_user_info.segment_email.length;
      }
      else{
        this.segment_user_info = {
          segment_email : 0,
          email_enable_count : 0,
          push_enable_count : 0,
          sms_enable_count : 0
      };
      }
    }); 

  }

  toggleSegmentType(type) {

    this.active_tab = type;


    if (type === 'recent') {

      this.creation_payload.recent_activity = {
        event_name: '',
        date_range: new Array(2)
      };

      // this.recent_segment = !this.recent_segment

      // if (!this.recent_segment) {
      //   this.creation_payload.recent_activity = {
      //     event_name: '',
      //     date_range: new Array(2)
      //   };
      // }

      //  else {
      //   delete this.creation_payload.recent_activity;
      // }
    } 
    else if (type === 'lifetime') {


      this.creation_payload.lifetime_activity = {
        field_name: '',
        condition_name: '',
        value: ''
      };

      // this.lifetime_segment = !this.lifetime_segment;
      // if (!this.lifetime_segment) {
      //   this.creation_payload.lifetime_activity = [
      //     {
      //       field_name: '',
      //       condition_name: '',
      //       value: ''
      //     }
      //   ];
      // } 

      // else {
      //   delete this.creation_payload.lifetime_activity;
      // }
    } else {

      this.creation_payload.demographic_activity = {
        gender: '',
        location_name: ''
      };

      // this.demographic_segment = !this.demographic_segment;
      // if (!this.demographic_segment) {
      //   this.creation_payload.demographic_activity = [
      //     {
      //       gender: '',
      //       location_name: ''
      //     }
      //   ];
      // } 

      // else {
      //   delete this.creation_payload.demographic_activity;
      // }
    }
  }

  ngOnInit() {

    // API to get Event List
    this.api.getEventList().subscribe(res => {
      if (!res['is_error']) {
        this.event_list = res['data'];
      }
    });

    // API to get City List
    this.api.getCityList().subscribe(res => {
      if (!res['is_error']) {
        this.city_list = res['data'];
      }
    });
    
  }

}
