import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatDialog } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';
import { Router } from '../../../../node_modules/@angular/router';
import { SendEmailModalComponent } from '../../modals/send-email-modal/send-email-modal.component';
import { SharedService } from '../../shared.service';
import { EmailPreviewComponent } from '../../shared/email-preview/email-preview.component';
import { EmailPreviewModalComponent } from '../../modals/email-preview-modal/email-preview-modal.component';

@Component({
  selector: 'app-email',
  templateUrl: './email.component.html',
  styleUrls: ['./email.component.css']
})
export class EmailComponent implements OnInit {

  dataSource: any;
  displayedColumns = ['id', 'progress', 'name', 'action'];
  tableData = [];
  spinner = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private api: ApiService,
    private router: Router,
    private dialog: MatDialog,
    private $root: SharedService
  ) { }

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  fireEmailCreation() {
    this.router.navigate(['home/create-email']);
  }
  fireSendAction(data) {
    const dialogRef = this.dialog.open(SendEmailModalComponent, {
      height: '300px',
      width: '600px',
      data: {
        subject: data.subject,
        content: data.template_content,
        name: data.template_name
      }
    });
  }

  fireCloneAction(data) {
    // this.$root.email_clone_data = data;
    this.router.navigate(['home/create-email/' + data.template_name]);
  }

  firePreviewAction(row) {
    const dialogRef = this.dialog.open(EmailPreviewModalComponent, {
      height: '300px',
      width: '600px',
      data: row.template_content
    });
  }

  fireEditAction(data){
    this.$root.edit_data  = data;
    this.router.navigate(['home/edit-email/' + data.template_name]);
  }

  ngOnInit() {

    this.api.getEmailTemplates().subscribe(res => {

      this.dataSource = new MatTableDataSource(res['data']);
      this.tableData = res['data'];
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinner = false;

      if(res['next_url']){
        this.getNextEmailList(res['next_url']);
      }

    })

  }

  getNextEmailList(url){

    this.api.getNextEmailTemplateList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.tableData.push(element);
      });

      if(res['next_url']){
        this.getNextEmailList(res['next_url']);
      }
      else{
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    });

  }

}
