import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../api.service';
import { MatSnackBar, MatDialog } from '../../../../node_modules/@angular/material';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { SendEmailModalComponent } from '../../modals/send-email-modal/send-email-modal.component';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-create-email',
  templateUrl: './create-email.component.html',
  styleUrls: ['./create-email.component.css']
})
export class CreateEmailComponent implements OnInit, OnDestroy {
  edit_mode = false;

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private $root: SharedService
  ) { 
    if(this.route.params['value']['name']){
      this.getParticularEmailTemplateData(this.route.params['value']['name']);
  }
  }

  template_content = '<h1>Hello</h1>';
  template_name: any;
  subject: any;

  subject_error = false;
  template_name_error = false;

    // START => get information about particular sms template
    getParticularEmailTemplateData(template_name){

      this.api.getParticularEmailData(template_name).subscribe(res => {

        if(this.router.url.indexOf('/home/edit-email') >= 0){
          this.subject = res['data'][0].subject;
      this.template_content = res['data'][0].template_content;
      this.template_name = res['data'][0].template_name;
          this.edit_mode = true;
        }
        else{

          this.subject = res['data'][0].subject;
      this.template_content = res['data'][0].template_content;
      this.template_name = 'Clone Of' + res['data'][0].template_name;
        }

      });

  }
  // END => get information about particular sms template

  validateEmailCreationForm() {
    let error = false;
    if (this.subject === '' || this.subject === undefined) {
      this.subject_error = true;
      error = true;
    }
    if (this.template_name === '' || this.template_name === undefined) {
      this.template_name_error = true;
      error = true;
    }
    if (this.template_content === '' || this.template_name === undefined) {
      error = true;
    }
    return error;
  }

  createEmailTemplate() {
    if (!this.validateEmailCreationForm()) {
      const payload = {
        subject: this.subject,
        template_content: this.template_content,
        template_name: this.template_name
      };
      this.api.createEmailTemplate(payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Template Saved Successfully', 'close', {
            duration: 2000
          });
          this.router.navigate(['home/email']);
        } else {
          this.snackbar.open(res['message'], 'close', {
            duration: 2000
          });
          this.router.navigate(['home/email']);
        }
      });
    }
  }

  saveAndSendEmail() {
    if (!this.validateEmailCreationForm()) {
      const payload = {
        subject: this.subject,
        template_content: this.template_content,
        template_name: this.template_name
      };
      this.api.createEmailTemplate(payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Template Saved Successfully', 'close', {
            duration: 2000
          });
          const sendEmailDialogRef = this.dialog.open(SendEmailModalComponent, {
            height: '300px',
            width: '600px',
            data: {
              subject: this.subject,
              content: this.template_content,
              name: this.template_name
            }
          });
          sendEmailDialogRef.afterClosed().subscribe(result => {
            if (result['data'] === 1) {
              this.router.navigate(['home/email']);
            }
          });
        } else {
          this.snackbar.open(res['message'], 'close', {
            duration: 2000
          });
          this.router.navigate(['home/email']);
        }
      });
    }
  }

  updateEmailTemplate() {
    const payload = {
      template_content: this.template_content,
      subject: this.subject
    };
    this.api.updateEmailTemplate(this.template_name, payload).subscribe(res => {
      if (!res['is_error']) {
        this.snackbar.open('Your Template Was Succesfully Updated.', 'Close', {
          duration: 2000
        });
        this.router.navigate(['home/email']);
      } else {
        this.snackbar.open(res['message'], 'Close', {
          duration: 2000
        });
        this.router.navigate(['home/email']);
      }
    })
  }

  ngOnInit() {
    // if (this.$root.email_clone_data !== undefined) {
    //   this.subject = this.$root.email_clone_data.subject;
    //   this.template_content = this.$root.email_clone_data.template_content;
    //   this.template_name = 'Clone Of' + this.$root.email_clone_data.template_name;
    // }
    // if (this.router.url === '/home/edit-email') {
    //   if (this.$root.edit_data !== undefined) {
    //     this.edit_mode = true;
    //     this.template_name = this.$root.edit_data.template_name;
    //     this.template_content = this.$root.edit_data.template_content;
    //     this.subject = this.$root.edit_data.subject;
    //   } 
    // }
  }

  ngOnDestroy() {
    this.$root.edit_data = undefined;
    this.$root.clone_data = undefined;
  }

}
