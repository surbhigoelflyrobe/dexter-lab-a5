import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiService } from '../../api.service';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog } from '../../../../node_modules/@angular/material';
import { Router } from '../../../../node_modules/@angular/router';
import { SendSmsModalComponent } from '../../modals/send-sms-modal/send-sms-modal.component';
import { SendPushModalComponent } from '../../modals/send-push-modal/send-push-modal.component';
import { SharedService } from '../../shared.service';
import { PushNotifPreviewModalComponent } from '../../modals/push-notif-preview-modal/push-notif-preview-modal.component';

@Component({
  selector: 'app-push-notifs',
  templateUrl: './push-notifs.component.html',
  styleUrls: ['./push-notifs.component.css']
})
export class PushNotifsComponent implements OnInit {
  push_data: any;
  push_data_length: any;
  dataSource: any;
  displayedColumns = ['id', 'name', 'progress', 'color', 'created', 'action'];
  tableData = [];
  spinner = true;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(
    private api: ApiService,
    private router: Router,
    private dialog: MatDialog,
    private $root: SharedService
  ) { }

  template_type_codes = [
    'text_no_action',
    'text_one_action',
    'text_two_action',
    'image_no_action',
    'image_one_action',
    'image_two_action'
  ];

  applyFilter(filterValue) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  firePushCreation() {
    this.router.navigate(['home/create-push']);
  }

  sendPushAction(row) {
    const dialogRef = this.dialog.open(SendPushModalComponent, {
      height: '300px',
      width: '600px',
      data: {
        name: row.template_name,
        type: row.template_content.template_type,
        body: row.template_content.content.body,
        title: row.template_content.content.title,
        deeplink: row.template_content.content.deeplink,
        big_image: row.template_content.content.big_image,
        large_icon: row.template_content.content.large_icon,
      }
    });
  }

  fireEditAction(data) {
    this.$root.edit_data = data;
    this.router.navigate(['home/edit-push/' + data.template_name]);
  }

  fireCloneAction(data) {
    // this.$root.clone_data = data;
    this.router.navigate(['home/create-push/' + data.template_name]);
  }

  firePreviewAction(data) {

    data['selected_type'] = this.template_type_codes.indexOf(data.template_content.template_type);

    this.dialog.open(PushNotifPreviewModalComponent, {
      height: 'auto',
      width: '260px',
      data: data
    });
  }

  ngOnInit() {
    this.api.getPushNotifs().subscribe(res => {
      this.push_data = res['data'];
      this.tableData = res['data'];
      this.push_data_length = res['data'].length;
      this.dataSource = new MatTableDataSource(this.push_data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.spinner = false;

      if(res['next_url']){
        this.getNextPNList(res['next_url']);
      }

    });
  }

  getNextPNList(url){

    this.api.getNextPNList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.tableData.push(element);
      });

      if(res['next_url']){
        this.getNextPNList(res['next_url']);
      }
      else{
        this.dataSource = new MatTableDataSource(this.tableData);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      }

    });

  }

}
