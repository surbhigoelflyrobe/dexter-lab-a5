import { Component, OnInit, OnDestroy } from '@angular/core';
import { ApiService } from '../../api.service';
import { MatSnackBar, MatDialog } from '../../../../node_modules/@angular/material';
import { Router , ActivatedRoute } from '../../../../node_modules/@angular/router';
import { SendPushModalComponent } from '../../modals/send-push-modal/send-push-modal.component';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-create-push',
  templateUrl: './create-push.component.html',
  styleUrls: ['./create-push.component.css']
})
export class CreatePushComponent implements OnInit, OnDestroy {

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private $root: SharedService
  ) { 
    if(this.route.params['value']['name']){
      this.getParticularPNTemplateData(this.route.params['value']['name']);
  }
  }

  template_types = [
    'Text No Action',
    'Text With One Action',
    'Text With Two Actions',
    'Image With No Action',
    'Image With One Action',
    'Image With Two Actions'
  ];
  template_type_codes = [
    'text_no_action',
    'text_one_action',
    'text_two_action',
    'image_no_action',
    'image_one_action',
    'image_two_action'
  ];
  selected_type = -1;
  edit_mode = false;

  creation_payload = {
    template_name: '',
    number_of_user: 1,
    selected_type : -1,
    template_content: {
      email: [],
      template_type: '',
      triggered_from: 'dashboard',
      segment_name: '',
      content: {
        title: '',
        body: '',
        deeplink: '',
        big_image: '',
        large_icon: ''
      }
    }
  };

  form_errors = {
    name: false,
    type: false,
    title: false,
    iconurl: false,
    deeplink: false,
    image: false,
    content: false
  };

  // START => get information about particular PN template
  getParticularPNTemplateData(template_name){

    this.api.getParticularPNData(template_name).subscribe(res => {

      console.log(res);

      if(this.router.url.indexOf('/home/edit-push') >= 0){
        this.creation_payload.template_name = res['data'][0].template_name;
        this.creation_payload.template_content = res['data'][0].template_content;
        this.creation_payload.selected_type = this.template_type_codes.indexOf(this.$root.edit_data.template_content.template_type);
        this.edit_mode = true;
      }
      else{
        this.creation_payload.template_name = "Clone_of_" + res['data'][0].template_name;
        this.creation_payload.template_content.content = res['data'][0].template_content.content;
        this.creation_payload.selected_type = this.template_type_codes.indexOf(res['data'][0].template_content.template_type);
      }

      console.log(" this.creation_payload ",  this.creation_payload);

    });

  }
  // END => get information about particular PN template

  validatePushForm() {
    let error = false;
    if (this.creation_payload.template_name === '') {
      error = true;
      this.form_errors.name = true;
    }
    if (this.creation_payload.template_content.content.title === '') {
      error = true;
      this.form_errors.title = true;
    }
    if (this.creation_payload.template_content.content.large_icon === '') {
      error = true;
      this.form_errors.iconurl = true;
    }
    if (this.creation_payload.template_content.content.deeplink === '') {
      error = true;
      this.form_errors.deeplink = true;
    }
    if (this.creation_payload.template_content.content.body === '') {
      error = true;
      this.form_errors.content = true;
    }

    if (this.creation_payload.selected_type > 2 && this.creation_payload.template_content.content.big_image === "") {
      error = true;
      this.form_errors.image = true;
    }

    return error;
  }

  // START -> function to create PN
  savePushNotification() {

    var error = this.validatePushForm();

    console.log("error ",error);

    if(error){
      if(this.creation_payload.template_name === "" || this.creation_payload.template_content.content.title === "" ||
      this.creation_payload.template_content.content.large_icon === "" || this.creation_payload.template_content.content.deeplink === "" ||
      this.creation_payload.template_content.content.body === "" ||
    (this.creation_payload.selected_type > 2 && this.creation_payload.template_content.content.big_image === "")){
        this.snackbar.open('Please enter required fields.', 'Close', {
          duration: 3000
        });
      }
    }
    else{
      this.creation_payload.template_content.template_type = this.template_type_codes[this.creation_payload.selected_type];

      this.api.createPushNotification(this.creation_payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Push Notification was saved', 'close', {
            duration: 2000
          });
          this.router.navigate(['home/push']);
        } else {
          this.snackbar.open(res['message'], 'close', {
            duration: 2000
          });
        }
      });

    }//end of else

  }
  // END -> function to create PN

  updatePushNotification() {
    if (!this.validatePushForm()) {
      this.api.updatePushTemplate(this.creation_payload.template_name, this.creation_payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Push Notification was Updated', 'close', {
            duration: 2000
          });
          this.router.navigate(['home/push']);
        } else {
          this.snackbar.open(res['message'], 'close', {
            duration: 2000
          });
        }
      });
    }
  }

  saveAndSendPushNotification() {

    var error = this.validatePushForm();

    if(error){
      if(this.creation_payload.template_name === "" || this.creation_payload.template_content.content.title === "" ||
      this.creation_payload.template_content.content.large_icon === "" || this.creation_payload.template_content.content.deeplink === "" ||
      this.creation_payload.template_content.content.body === "" ||
    (this.creation_payload.selected_type > 2 && this.creation_payload.template_content.content.big_image === "")){
        this.snackbar.open('Please enter required fields.', 'Close', {
          duration: 3000
        });
      }
    }
    else{
      this.creation_payload.template_content.template_type = this.template_type_codes[this.creation_payload.selected_type];
      this.api.createPushNotification(this.creation_payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Push Notification was saved', 'close', {
            duration: 2000
          });
          const dialogRef = this.dialog.open(SendPushModalComponent, {
            height: '300px',
            width: '600px',
            data: {
              name: this.creation_payload.template_name,
              type: this.creation_payload.template_content.template_type,
              body: this.creation_payload.template_content.content.body,
              title: this.creation_payload.template_content.content.title,
              deeplink: this.creation_payload.template_content.content.deeplink,
              big_image: this.creation_payload.template_content.content.big_image,
              large_icon: this.creation_payload.template_content.content.large_icon,
            }
          });
          dialogRef.afterClosed().subscribe(res => {
            this.router.navigate(['home/push']);
          });
        } else {
          this.snackbar.open(res['message'], 'close', {
            duration: 2000
          });
        }
      });
    }
  }


  ngOnInit() {
    // if (this.$root.clone_data !== undefined) {
    //   const data = this.$root.clone_data;
    //   this.creation_payload.template_name = 'Clone Of ' + data.template_name;
    //   this.creation_payload.template_content = data.template_content;
    //   this.creation_payload.selected_type = this.template_type_codes.indexOf(data.template_content.template_type);
    // }
    // if (this.router.url === '/home/edit-push') {
    //   if (this.$root.edit_data !== undefined) {
    //     this.edit_mode = true;
    //     this.creation_payload.template_name = this.$root.edit_data.template_name;
    //     this.creation_payload.template_content = this.$root.edit_data.template_content;

    //     this.creation_payload.selected_type = this.template_type_codes.indexOf(this.$root.edit_data.template_content.template_type);
    //   }
    // }
  }

  ngOnDestroy() {
    this.$root.clone_data = undefined;
    this.$root.edit_data = undefined;
  }

}
