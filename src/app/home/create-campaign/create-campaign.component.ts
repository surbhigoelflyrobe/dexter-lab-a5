import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { SharedService } from '../../shared.service';
import { Router , ActivatedRoute } from '../../../../node_modules/@angular/router';
import { ApiService } from '../../api.service';
import { DatePipe } from '../../../../node_modules/@angular/common';
import { MatSnackBar , MatStepper, MatDialog } from '../../../../node_modules/@angular/material';
import {
  FormGroup,
  FormBuilder,
  Validators
} from '../../../../node_modules/@angular/forms';
import { PushNotifPreviewModalComponent } from '../../modals/push-notif-preview-modal/push-notif-preview-modal.component';
import { SmsPreviewModalComponent } from '../../modals/sms-preview-modal/sms-preview-modal.component';
import { EmailPreviewModalComponent } from '../../modals/email-preview-modal/email-preview-modal.component';

@Component({
  selector: 'app-create-campaign',
  templateUrl: './create-campaign.component.html',
  styleUrls: ['./create-campaign.component.css']
})
export class CreateCampaignComponent implements OnInit, OnDestroy {

  @ViewChild('stepper') private myStepper: MatStepper;
  campaign_type: any;
  available_channels = ['email', 'sms', 'push'];
  meridies = ['AM', 'PM'];

  selected_channels = [];
  channel_arr = [];
  html_channel_arr = [];

  min_date = new Date();
  start_date: any;
  end_date: any;
  is_recurring = '1';
  email_templates: any;
  sms_templates: any;
  push_templates: any;

  templates = {
    email: [],
    email2: [],
    sms: [],
    sms2: [],
    push_notification: [],
    push_notification2: []
  };

  creation_payload: any = {
    campaign_name: '',
    campaign_type: '',
    start_date: '',
    start_hours: '',
    start_minutes: '',
    start_meridies: '',
    end_hours: '',
    end_minutes: '',
    end_meridies: '',
    segment_or_event_name: '',
    deactivate_event: '',
    channel: []
  };
  campaign_triggers: any;

  email_channel = false;
  sms_channel = false;
  push_notification_channel = false;

  edit_mode = false;
  show_second_Step = false;

  isLinear = false;

  constructor(
    private $root: SharedService,
    private router: Router,
    private route: ActivatedRoute,
    private api: ApiService,
    private datePipe: DatePipe,
    private snackbar: MatSnackBar,
    private dialog: MatDialog,
    private _formBuilder: FormBuilder
  ) {
    if(this.route.params['value']['campaign_type']){
      this.campaign_type = this.route.params['value']['campaign_type'];
      this.is_recurring = "2";
  }
   }

   template_type_codes = [
    'text_no_action',
    'text_one_action',
    'text_two_action',
    'image_no_action',
    'image_one_action',
    'image_two_action'
  ];

  //funcitons
  /*
    addChannel()           -> Funciton to add and save channel
    createCampaign()       -> Funciton to create Campaign
    createChannel()        -> Function to create channel
    preview()              -> Function ti view templates(email,sms,pn)
    removeChannel()        -> Function to remove channel
    validateFirstStep()    -> Function to validate basic details
  */

  // validateCreationStepTwo() {
  //   let error = false;
  //   if (this.creation_payload.channel.length === 0) {
  //     error = true;
  //   } else {
  //     this.creation_payload.channel.forEach(elem => {
  //       if (elem.selected_template === '') {
  //         error = true;
  //       }
  //       if (elem.frequency_obj !== undefined && this.is_recurring == '1') {
  //         if (elem.frequency_obj.frequency === 0) {
  //           error = true;
  //         }
  //         if (elem.frequency_obj.frequency_time === 0) {
  //           error = true;
  //         }
  //         // if (elem.frequency_obj.frequency_count === 0) {
  //         //   error = true;
  //         // }
  //         if (elem.frequency_obj.frequency_unit === '') {
  //           error = true;
  //         }
  //       }
  //       if (elem.waiting_time !== undefined) {
  //         if (elem.waiting_time.value === -1) {
  //           error = true;
  //         }
  //         if (elem.waiting_time.unit === '') {
  //           error = true;
  //         }
  //         // if (elem.waiting_time.when === '') {
  //         //   error = true;
  //         // }
  //       }
  //     });
  //   }
  //   return error;
  // }

  validateFirstStep() {
    let error = false;
    let today_date = this.datePipe.transform(new Date(), 'yyyy-MM-dd');

    if (this.creation_payload.campaign_name === '' || this.creation_payload.campaign_name === null ||
      this.creation_payload.campaign_name === undefined) {
      error = true;
      this.snackbar.open('Please enter Campaign Name', 'close', {
        duration: 2000
      });
      return error;
    }

    if (this.creation_payload.start_date === "" || this.creation_payload.start_date === null ||
      this.creation_payload.start_date === undefined) {
      error = true;
      this.snackbar.open('Please select valid Start Date & Time', 'close', {
        duration: 2000
      });
      return error;
    }
    else {
      this.creation_payload.start_date = this.datePipe.transform(this.creation_payload.start_date, 'yyyy-MM-dd');
      if (this.creation_payload.start_date < today_date) {
        error = true;
        this.snackbar.open('Please select valid Start Date & Time', 'close', {
          duration: 2000
        });
        return error;
      }
    }

    if (this.creation_payload.start_hours === "" || this.creation_payload.start_hours === null ||
      this.creation_payload.start_hours === undefined || this.creation_payload.start_hours > 12) {
      error = true;
      this.snackbar.open(' Please select valid Start Date & Time', 'close', {
        duration: 4000
      });
      return error;
    }

    if (this.creation_payload.start_minutes === "" || this.creation_payload.start_minutes === null ||
      this.creation_payload.start_minutes === undefined || this.creation_payload.start_minutes > 60) {
      error = true;
      this.snackbar.open(' Please select valid Start Date & Time', 'close', {
        duration: 4000
      });
      return error;
    }

    if (this.creation_payload.start_meridies === "" || this.creation_payload.start_meridies === null ||
      this.creation_payload.start_meridies === undefined) {
      error = true;
      this.snackbar.open(' Please select valid Start Date & Time', 'close', {
        duration: 4000
      });
      return error;
    }

    today_date = this.datePipe.transform(new Date(), 'yyyy-MM-dd hh:mm a');
    let new_start_date = this.creation_payload.start_date + " " + this.creation_payload.start_hours + ":" + this.creation_payload.start_minutes +
      " " + this.creation_payload.start_meridies;
    new_start_date = this.datePipe.transform(new Date(new_start_date), 'yyyy-MM-dd hh:mm a');

    if(this.api.timesBetween(new_start_date,today_date) <= 0){
      error = true;
      this.snackbar.open(' Please select valid Start Date & Time', 'close', {
        duration: 4000
      });
      return error;
    }

    // for end date
    if (this.campaign_type !== 'one_time_triggered') {

      if (this.creation_payload.end_date === "" || this.creation_payload.end_date === null ||
        this.creation_payload.end_date === undefined) {
        error = true;
        this.snackbar.open('Please select valid End Date & Time', 'close', {
          duration: 2000
        });
        return error;
      }
      else {
        this.creation_payload.end_date = this.datePipe.transform(this.creation_payload.end_date, 'yyyy-MM-dd');
      }

      if (this.creation_payload.end_hours === "" || this.creation_payload.end_hours === null ||
      this.creation_payload.end_hours === undefined || this.creation_payload.end_hours > 12) {
      error = true;
      this.snackbar.open('Please select valid End Date & Time', 'close', {
        duration: 2000
      });
      return error;
    }

    if (this.creation_payload.end_minutes === "" || this.creation_payload.end_minutes === null ||
      this.creation_payload.end_minutes === undefined || this.creation_payload.end_minutes > 60) {
      error = true;
      this.snackbar.open('Please select valid End Date & Time', 'close', {
        duration: 2000
      });
      return error;
    }

    if (this.creation_payload.end_meridies === "" || this.creation_payload.end_meridies === null ||
      this.creation_payload.end_meridies === undefined) {
      error = true;
      this.snackbar.open('Please select valid End Date & Time', 'close', {
        duration: 2000
      });
      return error;
    }

    let new_end_date = this.creation_payload.end_date + " " + this.creation_payload.end_hours + ":" + this.creation_payload.end_minutes +
      " " + this.creation_payload.end_meridies;
      new_end_date = this.datePipe.transform(new Date(new_end_date), 'yyyy-MM-dd hh:mm a');

    if (this.api.timesBetween(new_end_date,new_start_date) <= 0) {
      error = true;
      this.snackbar.open('Please select valid End Date & Time', 'close', {
        duration: 2000
      });
      return error;
    }

    }

    if (this.creation_payload.segment_or_event_name === '' || this.creation_payload.segment_or_event_name === null ||
      this.creation_payload.segment_or_event_name === undefined) {
      error = true;
    }

    if (error) {
      return error;
    }
    else {
      this.show_second_Step = true;
    }

  }

  /* selectChannel(channel) {
    if (channel === 'email') {
      if (!this.email_channel) {
        if (this.campaign_type === 'segment_triggered') {
          this.creation_payload.channel.push({
            name: 'email',
            selected_template: '',
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        } else if (this.campaign_type === 'one_time_triggered') {
          this.creation_payload.channel.push({
            name: 'email',
            selected_template: ''
          });
        } else {
          this.creation_payload.channel.push({
            name: 'email',
            selected_template: '',
            waiting_time: {
              value: -1,
              unit: '',
              when: ''
            },
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        }
      } else {
        this.creation_payload.channel = this.creation_payload.channel.filter(
          p => p.name !== 'email'
        );
      }
    } else if (channel === 'sms') {
      if (!this.sms_channel) {
        if (this.campaign_type === 'segment_triggered') {
          this.creation_payload.channel.push({
            name: 'sms',
            selected_template: '',
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        } else if (this.campaign_type === 'one_time_triggered') {
          this.creation_payload.channel.push({
            name: 'email',
            selected_template: ''
          });
        } else {
          this.creation_payload.channel.push({
            name: 'sms',
            selected_template: '',
            waiting_time: {
              value: -1,
              unit: '',
              when: ''
            },
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        }
      } else {
        this.creation_payload.channel = this.creation_payload.channel.filter(
          p => p.name !== 'sms'
        );
      }
    } else {
      if (!this.push_channel) {
        if (this.campaign_type === 'segment_triggered') {
          this.creation_payload.channel.push({
            name: 'push_notification',
            selected_template: '',
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        } else if (this.campaign_type === 'one_time_triggered') {
          this.creation_payload.channel.push({
            name: 'email',
            selected_template: ''
          });
        } else {
          this.creation_payload.channel.push({
            name: 'push_notification',
            selected_template: '',
            waiting_time: {
              value: -1,
              unit: '',
              when: ''
            },
            frequency_obj: {
              frequency: 0,
              frequency_time: 0,
              frequency_count: 0,
              frequency_unit: ''
            }
          });
        }
      } else {
        this.creation_payload.channel = this.creation_payload.channel.filter(
          p => p.name !== 'push_notification'
        );
      }
    }
  }
*/

  createChannel(channel){

    if(this.channel_arr[channel]){
      delete this.channel_arr[channel];
    }
    else{
      
        if(this.campaign_type === 'event_triggered'){
          this.channel_arr[channel] = {
            'name' : channel,
            'frequency_obj' : {},
            'waiting_time' : {}
          };
        }
        else{
          this.channel_arr[channel] = {
            'name' : channel,
            'frequency_obj' : {}
          };
        }

    }

    this.html_channel_arr = [];

    for(let a in this.channel_arr){
      this.html_channel_arr.push(this.channel_arr[a]);
    }

  }

  addChannel(channel){

    if(!channel.selected_template){
      this.snackbar.open('Please select template', 'close', {
        duration: 2000
      });
      return false;
    }

    if(this.campaign_type === "event_triggered"){
      if(!channel.waiting_time || !channel.waiting_time.value || !channel.waiting_time.unit){
        this.snackbar.open('Please select waiting Time', 'close', {
          duration: 2000
        });
        return false;
      }
    }

    if(this.is_recurring === "1"){
      if(!channel.frequency_obj || !channel.frequency_obj.frequency || !channel.frequency_obj.frequency_time
        || !channel.frequency_obj.frequency_unit){
        this.snackbar.open('Please select frequency', 'close', {
          duration: 2000
        });
        return false;
      }
    }

    this.selected_channels.push(channel);

  }

  removeChannel(channel){

    this.selected_channels.splice(this.selected_channels.indexOf(channel),1);
    this.html_channel_arr.splice(this.html_channel_arr.indexOf(channel),1);
    delete this.channel_arr[channel.name]
    this[[channel.name] + '_channel'] = false;

  }

  preview(channel){

    if(channel.name === 'push_notification'){

      let data = channel.template;

      data['selected_type'] = this.template_type_codes.indexOf(channel.template.template_content.template_type);

      this.dialog.open(PushNotifPreviewModalComponent, {
        height: 'auto',
        width: '260px',
        data: data
      });

    }
    else if(channel.name === 'sms'){
      const dialogRef = this.dialog.open(SmsPreviewModalComponent, {
        height: 'auto',
        width: '260px',
        data: channel.template.template_content
      });
    }
    else if(channel.name === 'email'){
      const dialogRef = this.dialog.open(EmailPreviewModalComponent, {
        height: '300px',
        width: '600px',
        data: channel.template.template_content
      });
    }
  }

  createCampaign() {

  var error = this.validateFirstStep();

  let send_data = {};

  if(!error){

    send_data = {
			"campaign_name": this.creation_payload.campaign_name,
			"campaign_type": this.campaign_type,
			"start_date": {
				"date": this.creation_payload.start_date,
				"time": this.creation_payload.start_hours + ":" + this.creation_payload.start_minutes,
				"meridiem": this.creation_payload.start_meridies
			},
			"end_date": {
				"date": this.creation_payload.end_date,
				"time": this.creation_payload.end_hours + ":" + this.creation_payload.end_minutes,
				"meridiem": this.creation_payload.end_meridies
			},
			"segment_or_event_name": this.creation_payload.segment_or_event_name,
			"deactivate_event": this.creation_payload.deactivate_event,
			"repetition_type": (this.is_recurring === "1" ? 'recurring' : 'non_recurring'),
			"channel" : []
    };

    this.selected_channels.forEach(function(col){

      var ch = {
        name : col.name,
        selected_template : col.selected_template
      };

      if(col["waiting_time"] && col["waiting_time"]["unit"]){
        ch["waiting_time"] = {
          unit : col["waiting_time"]["unit"],
          value : parseInt(col["waiting_time"]["value"])
        };
      }

      if(col["frequency_obj"] && col["frequency_obj"]["frequency"]){
        ch["frequency_obj"] = {
          frequency : parseInt(col["frequency_obj"]["frequency"]),
          frequency_time : parseInt(col["frequency_obj"]["frequency_time"]),
          frequency_unit : col["frequency_obj"]["frequency_unit"],
          frequency_count : 0
        };
      }

      send_data["channel"].push(ch);

    });
    
    this.api.createCampaign(send_data).subscribe(res => {
      if (!res['is_error']) {
        this.snackbar.open('Campaign Created Succesfully', 'close', {
          duration: 2000
        });
        this.router.navigate(['home/campaigns']);
      } else {
        this.snackbar.open(res['message'], 'close', {
          duration: 2000
        });
      }
    });

  }


    // this.creation_payload.start_date = {
    //   date: this.datePipe.transform(this.start_date, 'yyyy-MM-dd'),
    //   time: '12:00',
    //   meridiem: 'pm'
    // };
    // this.creation_payload.end_date = {
    //   date: this.datePipe.transform(this.end_date, 'yyyy-MM-dd'),
    //   time: '12:00',
    //   meridiem: 'pm'
    // };
    // this.creation_payload.channel.forEach(element => {
    //   if (this.is_recurring === '1') {
    //     delete element.frequency_obj;
    //   }
    // });
    // this.api.createCampaign(this.creation_payload).subscribe(res => {
    //   if (!res['is_error']) {
    //     this.snackbar.open('Campaign Created Succesfully', 'close', {
    //       duration: 2000
    //     });
    //     this.router.navigate(['home/campaigns']);
    //   } else {
    //     this.snackbar.open('An error occured. Try Later.', 'close', {
    //       duration: 2000
    //     });
    //   }
    // });
  }

  ngOnInit() {

    this.api.getEmailTemplates().subscribe(res => {

      if (res['data'] !== undefined) {
        this.templates.email = res['data'];
        this.templates.email2 = res['data'];
      }

      if(res['next_url']){
        this.getNextEmailList(res['next_url']);
      }

    });

    this.api.getSmsData().subscribe(res => {
      
      if (res['data'] !== undefined) {
        this.templates.sms = res['data'];
        this.templates.sms2 = res['data'];
      }

      if(res['next_url']){
        this.getSMSNextList(res['next_url']);
      }

    });

    this.api.getPushNotifs().subscribe(res => {

      if (res['data'] !== undefined) {
        this.templates.push_notification = res['data'];
        this.templates.push_notification2 = res['data'];
      }

      if(res['next_url']){
        this.getNextPNList(res['next_url']);
      }

    });

    if (this.campaign_type === 'segment_triggered' || this.campaign_type === 'one_time_triggered') {
      this.api.getSegments().subscribe(res => {
        this.campaign_triggers = res['data'];
      });
    }
    else if (this.campaign_type === 'event_triggered') {
      this.api.getFieldList('event').subscribe(res => {
        this.campaign_triggers = res['data'];
      });
    }

  }

  //function to get next sms list
  getSMSNextList(url){

    this.api.getNextSmsList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.templates.sms2.push(element);
      });

      if(res['next_url']){
        this.getSMSNextList(res['next_url']);
      }
      else{
        this.templates.sms = this.templates.sms2;
      }

    });

  }

   //function to get next email list
  getNextEmailList(url){

    this.api.getNextEmailTemplateList(url).subscribe(res => {
      res['data'].forEach(element => {
        this.templates.email2.push(element);
      });

      if(res['next_url']){
        this.getNextEmailList(res['next_url']);
      }
      else{
        this.templates.email = this.templates.email2
      }

    });

  }

  //function to get next PN list
  getNextPNList(url){

    this.api.getNextPNList(url).subscribe(res => {

      res['data'].forEach(element => {
        this.templates.push_notification2.push(element);
      });

      if(res['next_url']){
        this.getNextPNList(res['next_url']);
      }
      else{
        this.templates.push_notification = this.templates.push_notification2;
      }

    });

  }



  ngOnDestroy() {
    this.$root.create_campaign_type = '';
  }

}
