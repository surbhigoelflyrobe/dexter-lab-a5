import { Component, OnInit, OnDestroy } from '@angular/core';
import { MatDialog, MatSnackBar } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';
import { Router, ActivatedRoute } from '../../../../node_modules/@angular/router';
import { SendSmsModalComponent } from '../../modals/send-sms-modal/send-sms-modal.component';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-create-sms',
  templateUrl: './create-sms.component.html',
  styleUrls: ['./create-sms.component.css']
})
export class CreateSmsComponent implements OnInit, OnDestroy {

  title: any;
  content: any;
  edit_mode: boolean;

  constructor(
    private api: ApiService,
    private snackbar: MatSnackBar,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: MatDialog,
    private $root: SharedService
  ) { 
    if(this.route.params['value']['name']){
        this.getParticularSMSTemplateData(this.route.params['value']['name']);
    }
  }


  form_errors = {
    name: false,
    content: false,
    content_length_error : false
  };

  // START => get information about particular sms template
  getParticularSMSTemplateData(template_name){

      this.api.getParticularSmsData(template_name).subscribe(res => {

        if(this.router.url.indexOf('/home/edit-sms') >= 0){
          this.title = res['data'][0].template_name;
          this.edit_mode = true;
        }
        else{
          this.title = "Clone_of_" + res['data'][0].template_name;
        }

        this.content = res['data'][0].template_content;

      });

  }
  // END => get information about particular sms template

  validateSmsForm() {

    let error = false;
    if (this.title === '' || this.title === undefined) {
      error = true;
      this.form_errors.name = true;
    }

    if (this.content === '' || this.content === undefined) {
      error = true;
      this.form_errors.content = true;
      return error;
    }

    if (this.content && this.content.length > 250) {
      error = true;
      this.form_errors.content_length_error = true;
    }

    return error;
  }

  createSmsTemplate() {

    var res = this.validateSmsForm();

    if (!res) {
      const payload = {
        template_name: this.title,
        template_content: this.content
      };

      this.api.createSmsTemplate(payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('SMS TEmplate Saved Succesfully', 'Close', {
            duration: 2000
          });
          this.router.navigate(['home/sms']);
        } else {
          this.snackbar.open(res['message'], 'Close', {
            duration: 2000
          });
        }
      });
    }
    else{
      if(this.form_errors.name && this.form_errors.content){
        this.snackbar.open('Please enter required fields.', 'Close', {
          duration: 3000
        });
      }
    }
  }

  saveAndSendSms() {

    var res = this.validateSmsForm();

    if (!res) {
      const payload = {
        template_name: this.title,
        template_content: this.content
      };
      this.api.createSmsTemplate(payload).subscribe(res => {

        if (!res['is_error']) {//successful div
          this.snackbar.open('SMS Template Saved Succesfully', 'Close', {
            duration: 2000
          });
          const dialogRef = this.dialog.open(SendSmsModalComponent, {
            height: '300px',
            width: '600px',
            data: {
              content: this.content,
              name: this.title
            }
          });
          dialogRef.afterClosed().subscribe(result => {
            this.router.navigate(['home/sms']);
          });
        } else {//error div
          this.snackbar.open('' + res['message'], 'Close', {
            duration: 2000
          });
        }

      });//end of API
    }
    else{
      if(this.form_errors.name && this.form_errors.content){
        this.snackbar.open('Please enter required fields.', 'Close', {
          duration: 3000
        });
      }
    }

  }

  updateSmsTemplate() {
    if (!this.validateSmsForm()) {
      const payload = {
        template_content: this.content
      };
      this.api.updateSmsTemplate(this.title, payload).subscribe(res => {
        if (!res['is_error']) {
          this.snackbar.open('Your Template Was Succesfully Updated.', 'Close', {
            duration: 2000
          });
          this.router.navigate(['home/sms']);
        } else {
          this.snackbar.open(res['message'], 'Close', {
            duration: 2000
          });
          this.router.navigate(['home/sms']);
        }
      });
    }
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.$root.clone_data = undefined;
    this.$root.edit_data = undefined;
  }

}
