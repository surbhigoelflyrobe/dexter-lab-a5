import { HttpClient }          from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
 
@Pipe({
  name: 'limitTo',
  pure: false
})
export class LimitToPipe implements PipeTransform {
 
  constructor(private http: HttpClient) { }
 
  transform(st: string, l: number): any {
    if(st.length > l){
        return st.substr(0,l);
    }
 
    return st;
  }
}