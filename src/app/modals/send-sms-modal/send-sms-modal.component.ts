import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-send-sms-modal',
  templateUrl: './send-sms-modal.component.html',
  styleUrls: ['./send-sms-modal.component.css']
})
export class SendSmsModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SendSmsModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private api: ApiService,
    private snackbar: MatSnackBar
  ) { }

  target_user_group: any;
  user_numbers = '';
  selected_segment: any;
  segments_list = [];
  template_name: any;

  sending_payload = {
    mobile: [],
    content: ''
  };

  form_errors = {
    target_users: false,
    specific_user_no: false,
    segment : false
  };

  //START -> Function to check whether form is valid or not
  checkForm(){

    let error = false;
    if (this.target_user_group === '' || this.target_user_group === undefined) {
      error = true;
      this.form_errors.target_users = true;
      return error;
    }

    if(this.target_user_group == 1){//specific users
      if(this.user_numbers === '' || this.user_numbers === null || this.user_numbers === undefined){
        error = true;
      this.form_errors.specific_user_no = true;
      return error;
      }
    }
    else if(this.target_user_group == 2){//segment users

      if(this.selected_segment === '' || this.selected_segment === null || this.selected_segment === undefined){
        error = true;
      this.form_errors.segment = true;
      return error;
      }
    }

  }
  //END -> Function to check whether form is valid or not

  sendSms() {

    var error = this.checkForm();

    console.log("this.form_errors ",this.form_errors);

    if(error){
      if(this.form_errors.target_users || (this.form_errors.specific_user_no || this.form_errors.segment)){
        this.snackbar.open('Please select required fields.', 'Close', {
          duration: 3000
        });
      }
    }
    else{

      if(this.target_user_group == 1){//specific users
        this.sending_payload.mobile = this.user_numbers.split(',');
      }
      else if(this.target_user_group == 2){//segment users
        this.sending_payload.mobile = this.selected_segment.segment_mobile;
      }
  
      this.api.sendSms(this.sending_payload).subscribe(res => {
        if (!res['is_error']) {
          this.dialogRef.close({ data: 1 });
          this.snackbar.open(res['data'], 'Close', {
            duration: 2000
          });
        } else {
          this.dialogRef.close({ data: 0 });
          this.snackbar.open(res['message'], 'Close', {
            duration: 2000
          });
        }
      })
    
    }

   
  }

  onNoClick() {
    this.dialogRef.close({ data: 0 });
  }

  ngOnInit() {
    this.sending_payload.content = this.data.content;
    this.template_name = this.data.name;
    this.api.getSegments().subscribe(res => {
      if (!res['is_error']) {
        this.segments_list = [];

        res['data'].forEach(element => {
          if(element.segment_mobile.length > 0 && element.segment_mobile[0] != null){
            this.segments_list.push(element);
          }
        });
      }
    });
  }

}
