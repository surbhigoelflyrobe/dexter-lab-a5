import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-send-email-modal',
  templateUrl: './send-email-modal.component.html',
  styleUrls: ['./send-email-modal.component.css']
})
export class SendEmailModalComponent implements OnInit {
  segments_list: any;
  target_user_group: any;
  user_emails: any;
  selected_segment: any;
  full_email_list: any;
  template_name: any;

  sending_payload = {
    to_emails: [],
    text_or_email: '',
    subject: ''
  };

  constructor(
    public dialogRef: MatDialogRef<SendEmailModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private api: ApiService,
    private snackbar: MatSnackBar
  ) { }

  sendEmail() {
    if (this.target_user_group == 0) {
      this.sending_payload.to_emails = this.full_email_list;
    } else if (this.target_user_group == 1) {
      this.sending_payload.to_emails = this.user_emails.split(',');
    } else {
      this.sending_payload.to_emails = this.selected_segment.segment_email;
    }
    this.api.sendEmail(this.sending_payload).subscribe(res => {
      if (!res['is_error']) {
        this.dialogRef.close({data: 1});
        this.snackbar.open('Your Emails were delivered', 'Close', {
          duration: 2000
        });
      } else {
        this.snackbar.open('An Error Occured. Try Later', 'Close', {
          duration: 2000,
          panelClass: 'error-snack'
        });
      }
    });
  }

  onNoClick() {
    this.dialogRef.close({ data: 0 });
  }


  ngOnInit() {
    this.api.getSegments().subscribe(res => {
      if (!res['is_error']) {
        this.segments_list = res['data'];
      }
    });
    this.api.getAllEmails().subscribe(res => {
      if (!res['is_error']) {
        this.full_email_list = res['data'];
      }
    });
    this.sending_payload.subject = this.data.subject;
    this.sending_payload.text_or_email = this.data.content;
    this.template_name = this.data.name;

  }

}
