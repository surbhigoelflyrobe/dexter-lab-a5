import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-push-notif-preview-modal',
  templateUrl: './push-notif-preview-modal.component.html',
  styleUrls: ['./push-notif-preview-modal.component.css']
})
export class PushNotifPreviewModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<PushNotifPreviewModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private api: ApiService,
    private snackbar: MatSnackBar
  ) { }


  ngOnInit() {
    console.log(this.data)
  }

}
