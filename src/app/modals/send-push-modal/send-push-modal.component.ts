import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '../../../../node_modules/@angular/material';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-send-push-modal',
  templateUrl: './send-push-modal.component.html',
  styleUrls: ['./send-push-modal.component.css']
})
export class SendPushModalComponent implements OnInit {
  segments_list: any;
  full_email_list: any;

  constructor(
    public dialogRef: MatDialogRef<SendPushModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    private api: ApiService,
    private snackbar: MatSnackBar
  ) { }

  target_user_group: any;
  user_emails: any;
  selected_segment: any;

  sending_payload = {
    email: [
    ],
    template_name: '',
    template_type: '',
    triggered_from: 'dashboard',
    segment_name: 'test_segment1',
    content: {
      title: '',
      body: '',
      deeplink: '',
      big_image: '',
      large_icon: ''
    }
  };

  sendPushNotif() {
    if (this.target_user_group == 2) {
      this.sending_payload.segment_name = this.selected_segment.segment_name;
      this.sending_payload.email = this.selected_segment.segment_email;
    }
    if (this.target_user_group == 0) {
      this.sending_payload.email = this.full_email_list;
    }
    if (this.target_user_group == 1) {
      this.sending_payload.email = this.user_emails.split(',');
    }

    this.sending_payload.template_name = "xuddsvs";

    this.api.sendPushNotification(this.sending_payload).subscribe(res => {
      if (!res['is_error']) {
        this.snackbar.open('Your Push Notifs Were Sent.', 'Close', {
          duration: 2000
        });
        this.dialogRef.close()
      } else {
        this.snackbar.open('An Error Occured. Try Later.', 'Close', {
          duration: 2000
        });
        this.dialogRef.close()
      }
    }
    );
  }

  ngOnInit() {
    this.api.getSegments().subscribe(res => {
      if (!res['is_error']) {
        this.segments_list = res['data'];
      }
    });
    this.api.getAllEmails().subscribe(res => {
      if (!res['is_error']) {
        this.full_email_list = res['data'];
      }
    });

    this.sending_payload.template_name = this.data.name;
    this.sending_payload.template_type = this.data.type;
    this.sending_payload.content.body = this.data.body;
    this.sending_payload.content.title = this.data.title;
    this.sending_payload.content.deeplink = this.data.deeplink;
    this.sending_payload.content.big_image = this.data.big_image;
    this.sending_payload.content.large_icon = this.data.large_icon;

  }

}
