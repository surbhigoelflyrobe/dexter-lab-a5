import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared.service';
// import { MatSlideToggleModule } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-switch-modal',
  templateUrl: './switch-modal.component.html',
  styleUrls: ['./switch-modal.component.css']
})
export class SwitchModalComponent implements OnInit {

  mode :number;

  constructor(private $root: SharedService) {
    // this.mode = $root.mode;
    console.log("constructor mode ",this.mode);
    console.log(window.localStorage.getItem('mode'));
    this.mode = parseInt(window.localStorage.getItem('mode'));
    $root.mode = this.mode;
   }

  ngOnInit() {
  }

  switchMode(value){

    if (value.checked == true) {
      this.mode = 1;
      console.log(1);
    } else {
      this.mode = 0;
      console.log(0);
    }

    window.localStorage.setItem('mode', "" + this.mode);

    window.location.reload();

    // console.log("mode ",this.mode);
  }

}
