import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-sms-preview-modal',
  templateUrl: './sms-preview-modal.component.html',
  styleUrls: ['./sms-preview-modal.component.css']
})
export class SmsPreviewModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<SmsPreviewModalComponent>,
    @Inject(MAT_DIALOG_DATA) public content,
  ) { }

  ngOnInit() {
  }

}
