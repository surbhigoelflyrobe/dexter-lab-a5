import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '../../../../node_modules/@angular/material';

@Component({
  selector: 'app-create-campaign-modal',
  templateUrl: './create-campaign-modal.component.html',
  styleUrls: ['./create-campaign-modal.component.css']
})
export class CreateCampaignModalComponent {

  constructor(
    public dialogRef: MatDialogRef<CreateCampaignModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  onNoClick(): void {
    this.dialogRef.close({});
  }
  dismiss() {
    this.dialogRef.close({ data: 0 });
  }
  segmentTriggered() {
    this.dialogRef.close({ data: 'segment_triggered' });
  }
  eventTriggered() {
    this.dialogRef.close({ data: 'event_triggered' });
  }
  oneTime() {
    this.dialogRef.close({ data: 'one_time_triggered' });
  }
}
