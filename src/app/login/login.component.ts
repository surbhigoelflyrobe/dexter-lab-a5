import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '../../../node_modules/@angular/router';
import { SharedService } from '../shared.service';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private api: ApiService,
    private router: Router,
    private $root: SharedService,
    private snackbar: MatSnackBar
  ) { }

  username: any;
  password: any;

  userLogin(username, pass) {
    this.api.userLogin(username, pass).subscribe(res => {
      if (!res['is_error']) {
        this.$root.mode = 0;
        window.localStorage.setItem('mode', "0");
        window.localStorage.setItem('pnd_token', res['data'].user_info.access_token);
        window.localStorage.setItem('user_data', JSON.stringify(res['data'].user_info.user_details));
        window.localStorage.setItem('permissions', JSON.stringify(res['data'].user_role_based_access[0].resource_access_permissions));
        this.$root.user_auth_token = res['data'].user_info.access_token;
        this.$root.user_data = res['data'].user_info.user_details;
        this.$root.permissions = res['data'].user_role_based_access[0].resource_access_permissions;
        this.router.navigate(['home']);
      } else {
        this.snackbar.open('Invalid Credentials. Try Again.', 'Close', {
          duration: 2000
        });
      }
    });
  }

  ngOnInit() {
  }

}
