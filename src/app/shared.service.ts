import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  user_auth_token = '';
  user_data: any;
  permissions: any;

  create_campaign_type = '';
  push_notif_data: any;
  email_clone_data: any;
  clone_data: any;
  edit_data: any;
  mode = 0;

  constructor() { }
}
