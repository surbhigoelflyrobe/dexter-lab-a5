import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AppSettings } from './app-settings';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  options = {};
  url = {};

  constructor(
    private http: HttpClient
  ) { 
    if(parseInt(window.localStorage.getItem('mode')) === 1){//live
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': '6cbx96xjjy7pljw'
        })
      };

      this.url = {
        ACL_ENDPOINT : 'https://node.flyrobeapp.com:8002',
        NODE_V1 : 'http://app-logs.flyrobeapp.com:3000/api/v1',
        NODE_V2 : 'http://app-logs.flyrobeapp.com:3000/api/v2'
      };
    }
    else{
      this.options = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          'Authorization': '1nhf2fhjjjh0hahzn'
        })
      };
      
      this.url = {
        ACL_ENDPOINT : 'https://node-staging.flyrobeapp.com:8002',
        NODE_V1 : 'http://node-staging.flyrobeapp.com:3000/api/v1',
        NODE_V2 : 'http://node-staging.flyrobeapp.com:3000/api/v2'
      };

    }
  }
  

  userLogin(email, password) {
    const payload = {
      grant_type: 'password',
      username: email,
      password: password,
      resource: 'PND'
    };
    return this.http.post(this.url['ACL_ENDPOINT'] + '/user-login', payload);
  }

  getLiveEvents() {
    return this.http.get(this.url['NODE_V1'] + '/get_event/?platform=Android&page=1&limit=50&sort_key=desc', this.options);
  }

  getCampaigns(size = 20) {
    return this.http.get(this.url['NODE_V1'] + '/get_campaigns?page=1&limit=' + size + '&sort_key=desc', this.options);
  }

     // API to get next Campaign list
  getNextCampaignList(url) {
    return this.http.get(url, this.options);
  }

  getEmailTemplates() {
    return this.http.get(this.url['NODE_V1'] + '/get_email_templates?page=1&limit=15', this.options);
  }

   // API to get next Email list
  getNextEmailTemplateList(url) {    
    return this.http.get(url, this.options);
  }

  getPushNotifs() {
    return this.http.get(this.url['NODE_V1'] + '/get_pn_templates?page=1&limit=15', this.options);
  }

  // API to get next PN list
  getNextPNList(url) {
    return this.http.get(url, this.options);
  }

  getSegments() {
    return this.http.get(this.url['NODE_V1'] + '/get_segments?page=1&limit=15', this.options);
  }

  getNextSegmentList(url) {
    return this.http.get(url, this.options);
  }

  getSegmentData(payload) {
    return this.http.post(this.url['NODE_V1'] + '/get_segment_data', payload,this.options);
  }

  getSmsData() {
    return this.http.get(this.url['NODE_V1'] + '/get_sms_templates?page=1&limit=15&sort_key=desc', this.options);
  }

  // API to get next SMS list
  getNextSmsList(url) {
    return this.http.get(url, this.options);
  }

  getParticularSmsData(payload) {
    return this.http.get(this.url['NODE_V1'] + '/get_sms_templates/' + payload, this.options);
  }

  getParticularPNData(payload) {
    return this.http.get(this.url['NODE_V1'] + '/get_pn_templates/' + payload, this.options);
  }

  getParticularEmailData(payload) {
    return this.http.get(this.url['NODE_V1'] + '/get_email_templates/' + payload, this.options);
  }

  createCampaign(payload) {
    return this.http.post(this.url['NODE_V1'] + '/create_campaign', payload, this.options);
  }

  createSmsTemplate(payload) {
    return this.http.post(this.url['NODE_V1'] + '/save_sms_templates', payload, this.options);
  }

  createEmailTemplate(payload) {
    return this.http.post(this.url['NODE_V1'] + '/save_email_templates', payload, this.options);
  }

  createSegment(payload) {
    return this.http.post(this.url['NODE_V1'] + '/create_segmentation', payload, this.options);
  }

  createPushNotification(payload) {
    return this.http.post(this.url['NODE_V1'] + '/save_pn_templates', payload, this.options);
  }

  getCityList() {
    return this.http.get(this.url['NODE_V1'] + '/get_city_list', this.options);
  }

  getEventList() {
    return this.http.get(this.url['NODE_V1'] + '/get_event_list/Android', this.options);
  }

  getFieldList(keyname) {
    return this.http.get(this.url['NODE_V1'] + '/get_field_list/?key_name=' + keyname + '&platform=Android', this.options);
  }

  getParticularSegmentData(payload){
    return this.http.get(this.url['NODE_V1'] + '/get_segments/' + payload, this.options);
  }

  getAllEmails() {
    return this.http.get(this.url['NODE_V1'] + '/all_user_email', this.options);
  }

  sendEmail(payload) {
    return this.http.post(this.url['NODE_V1'] + '/send_email', payload, this.options);
  }

  sendSms(payload) {
    return this.http.post(this.url['NODE_V1'] + '/send_sms', payload, this.options);
  }

  sendPushNotification(payload) {
    return this.http.post(this.url['NODE_V2'] + '/firebase_push', payload, this.options);
  }

  updateSmsTemplate(template_name, payload) {
    return this.http.put(this.url['NODE_V1'] + '/update_sms_template/' + template_name, payload, this.options);
  }

  updateEmailTemplate(template_name, payload) {
    return this.http.put(this.url['NODE_V1'] + '/update_email_template/' + template_name, payload, this.options);
  }

  updatePushTemplate(template_name, payload) {
    return this.http.put(this.url['NODE_V1'] + '/update_pn_template/' + template_name, payload, this.options);
  }

  updateSegment(segment_name, payload) {
    return this.http.put(this.url['NODE_V1'] + '/update_segment/' + segment_name, payload, this.options);
  }

  updateCampaign(campaign_name, payload) {
    return this.http.put(this.url['NODE_V1'] + '/update_campaign/' + campaign_name, payload, this.options);
  }

  timesBetween(start_date,end_date){

    console.log(start_date);
            console.log(end_date);

    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    start_date = new Date(start_date);
    end_date = new Date(end_date);

    // Convert both dates to milliseconds
    var date1_ms = start_date.getTime();
    var date2_ms = end_date.getTime();

    console.log(date1_ms);
    console.log(date2_ms);

    // Calculate the difference in milliseconds
    var difference_ms = date1_ms - date2_ms;

    // Convert back to days and return
    return Math.round(difference_ms);

  }


}
