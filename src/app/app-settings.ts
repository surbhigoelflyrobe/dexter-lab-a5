export class AppSettings {
    public static ACL_ENDPOINT = 'https://node-staging.flyrobeapp.com:8002';
    public static NODE_V1 = 'http://node-staging.flyrobeapp.com:3000/api/v1';
    public static NODE_V2 = 'http://node-staging.flyrobeapp.com:3000/api/v2';
}
