import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { AgGridModule } from 'ag-grid-angular';

import { NgModule } from '@angular/core';
import { OrderModule } from 'ngx-order-pipe';

import { AppComponent } from './app.component';

import { HashLocationStrategy, LocationStrategy } from '@angular/common';

import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule, MatButtonModule, MatExpansionModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MatTabsModule, MatSelectModule, MatDatepickerModule, MatNativeDateModule, MatRadioModule, MatStepperModule, MatSnackBarModule, MatCheckbox, MatCheckboxModule, MatIconModule, MatTooltipModule, MatCardModule } from '@angular/material';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
import { LiveEventsComponent } from './home/live-events/live-events.component';
import { PushNotifsComponent } from './home/push-notifs/push-notifs.component';
import { CampaignsComponent } from './home/campaigns/campaigns.component';
import { SegmentsComponent } from './home/segments/segments.component';
import { SmsComponent } from './home/sms/sms.component';
import { EmailComponent } from './home/email/email.component';
import { CreateCampaignComponent } from './home/create-campaign/create-campaign.component';
import { CreateCampaignModalComponent } from './modals/create-campaign-modal/create-campaign-modal.component';
import { CreateSegmentModalComponent } from './modals/create-segment-modal/create-segment-modal.component';
import { CreateSegmentComponent } from './home/create-segment/create-segment.component';
import { CreateEmailComponent } from './home/create-email/create-email.component';
import { CreateSmsComponent } from './home/create-sms/create-sms.component';
import { CreatePushComponent } from './home/create-push/create-push.component';

import { DatePipe } from '../../node_modules/@angular/common';
import { LimitToPipe } from './pipes/length.pipes';

import { NumberOnlyDirective } from './directives/number.directive';
import { TemplateNameOnlyDirective } from './directives/template_name.directive';

import { FroalaEditorModule, FroalaViewModule } from 'angular-froala-wysiwyg';
import { SendEmailModalComponent } from './modals/send-email-modal/send-email-modal.component';
import { SendSmsModalComponent } from './modals/send-sms-modal/send-sms-modal.component';
import { SendPushModalComponent } from './modals/send-push-modal/send-push-modal.component';
import { PushNotifPreviewComponent } from './shared/push-notif-preview/push-notif-preview.component';
import { PushNotifPreviewModalComponent } from './modals/push-notif-preview-modal/push-notif-preview-modal.component';
import { SmsPreviewComponent } from './shared/sms-preview/sms-preview.component';
import { SmsPreviewModalComponent } from './modals/sms-preview-modal/sms-preview-modal.component';
import { EmailPreviewModalComponent } from './modals/email-preview-modal/email-preview-modal.component';
import { EmailPreviewComponent } from './shared/email-preview/email-preview.component';
import { SwitchModalComponent } from './modals/switch-modal/switch-modal.component';




const routes: Routes = [
  { path: '', component: LoginComponent },
  {
    path: 'home', component: HomeComponent, children: [
      { path: '', component: LiveEventsComponent },
      { path: 'events', component: LiveEventsComponent },
      
      { path: 'push', component: PushNotifsComponent },
      { path: 'create-push', component: CreatePushComponent },
      { path: 'create-push/:name', component: CreatePushComponent },
      { path: 'edit-push/:name', component: CreatePushComponent },

      { path: 'campaigns', component: CampaignsComponent },
      { path: 'create-campaign/:campaign_type', component: CreateCampaignComponent },
      { path: 'edit-campaign', component: CreateCampaignComponent },

      { path: 'segments', component: SegmentsComponent },
      { path: 'create-segment', component: CreateSegmentComponent },
      { path: 'create-segment/:name', component: CreateSegmentComponent },
      { path: 'edit-segment/:name', component: CreateSegmentComponent },

      { path: 'sms', component: SmsComponent },
      { path: 'create-sms', component: CreateSmsComponent },
      { path: 'create-sms/:name', component: CreateSmsComponent },
      { path: 'edit-sms/:name', component: CreateSmsComponent },

      { path: 'email', component: EmailComponent },
      { path: 'create-email', component: CreateEmailComponent },
      { path: 'create-email/:name', component: CreateEmailComponent },
      { path: 'edit-email/:name', component: CreateEmailComponent },
    ]
  }
];

@NgModule({
  declarations: [
    LimitToPipe,
    NumberOnlyDirective,
    TemplateNameOnlyDirective,

    AppComponent,
    LoginComponent,
    HomeComponent,
    LiveEventsComponent,
    PushNotifsComponent,
    CampaignsComponent,
    SegmentsComponent,
    SmsComponent,
    EmailComponent,
    CreateCampaignComponent,
    CreateCampaignModalComponent,
    CreateSegmentModalComponent,
    CreateSegmentComponent,
    CreateEmailComponent,
    CreateSmsComponent,
    CreatePushComponent,
    SendEmailModalComponent,
    SendSmsModalComponent,
    SendPushModalComponent,
    PushNotifPreviewComponent,
    PushNotifPreviewModalComponent,
    SmsPreviewComponent,
    SmsPreviewModalComponent,
    EmailPreviewModalComponent,
    EmailPreviewComponent,
    SwitchModalComponent
  ],
  imports: [
    AgGridModule.withComponents(null),
    MatSlideToggleModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatMenuModule,
    RouterModule.forRoot(routes),
    MatSidenavModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatExpansionModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatDialogModule,
    MatTabsModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatRadioModule,
    MatStepperModule,
    MatSnackBarModule,
    MatCheckboxModule,
    MatIconModule,
    FroalaEditorModule.forRoot(),
    FroalaViewModule.forRoot(),
    MatTooltipModule,
    MatCardModule,
    OrderModule
  ],
  providers: [DatePipe, {provide : LocationStrategy, useClass : HashLocationStrategy}],
  bootstrap: [AppComponent],
  // tslint:disable-next-line:max-line-length
  entryComponents: [
    SendEmailModalComponent,
    CreateCampaignModalComponent,
    SendSmsModalComponent,
    SendPushModalComponent,
    PushNotifPreviewModalComponent,
    SmsPreviewModalComponent,
    EmailPreviewModalComponent,
    SwitchModalComponent
  ]
})
export class AppModule { }
