import { Component, OnInit } from '@angular/core';
import { SharedService } from './shared.service';
import { Router } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';

  constructor(
    private $root: SharedService,
    private router: Router
  ) {

  }

  ngOnInit() {
    if (window.localStorage.getItem('pnd_token') !== null) {
      this.$root.user_auth_token = window.localStorage.getItem('pnd_token');
    } else {
      this.router.navigate(['/']);
    }
    if (window.localStorage.getItem('user_data') !== null) {
      this.$root.user_data = JSON.parse(window.localStorage.getItem('user_data'));
    }
    if (window.localStorage.getItem('permissions') !== null) {
      this.$root.permissions = JSON.parse(window.localStorage.getItem('permissions'));
    }
  }
}
