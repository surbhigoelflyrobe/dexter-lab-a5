import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sms-preview',
  templateUrl: './sms-preview.component.html',
  styleUrls: ['./sms-preview.component.css']
})
export class SmsPreviewComponent implements OnInit {

  constructor() { }

  @Input() data: any;

  ngOnInit() {
  }

}
