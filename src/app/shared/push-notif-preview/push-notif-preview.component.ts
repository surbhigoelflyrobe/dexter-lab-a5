import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-push-notif-preview',
  templateUrl: './push-notif-preview.component.html',
  styleUrls: ['./push-notif-preview.component.css']
})
export class PushNotifPreviewComponent implements OnInit {

  constructor() { }
  @Input() data: any;

  selected_type: any;

  ngOnInit() {
    console.log(this.data);
  }

}
